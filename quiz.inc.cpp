{
    "Die Otter",
    {
        {"Der Papiermüll wird:", {
            {"Einfach entsorgt", false, 0},
            {"Getrennt entsorgt", false, 1},
            {"Zerkleinert und recycelt", true, 2}
        }, true, "<b>Tipp:</b> Es wird empfohlen, dass Papiermüll aus Umweltschutzgründen zerkleinert und recycled wird."},

        {"Die Software:", {
            {"Wird ohne weitere Energiesparmaßnahmen genutzt", false, 0},
            {"Wird teilweise Nachhaltig genutzt", false, 1},
            {"Ist nachhaltig", true, 2}
        }, true, "<b>Tipp:</b> Um Energie zu sparen, sollten energiesparende Programme (z.B. welche die mit dem Blauen Engel ausgezeichnet sind) verwendet werden."},

        {"Es wird folgendermaßen gelüftet:", {
            {"Regelmäßig", false, 0},
            {"Nach Plan", false, 1},
            {"Automatisch ohne Wärmeverlust", true, 2}
        }, true, "<b>Tipp:</b> Eine automatische Lüftung ist wichtig, da diese ohne Wärmeverlust stattfinden kann, was Energie spart."},

        {"Zu Ihrem Unternehmen gelangen Sie:", {
            {"Durch die ÖPNV, wie Regionalverkehr, S-Bahnen und Fernzüge, dem (Elektro-)Fahrrad oder zu Fuß.", true, 2},
            {"Mit dem eigenen Auto.", false, 0},
            {"Überwiegend mit dem Bus.", false, 1}
        }, true, "<b>Tipp:</b> Am besten in der Nähe der Arbeit wohnen oder öffentliche Verkehrsmittel verwenden, da sie großtenteils mit Strom betrieben werden, welcher keine Emissionen verursacht."},

        {"Grundlagen des Heizens in Ihrem Unternehmen wurden:", {
            {"Geprüft und es wurden auch schon Abdämmungsarbeiten vorgenommen.", true, 2},
            {"Mal nachgeprüft, aber das Projekt wurde nicht weiter verfolgt.", false, 1},
            {"Bisher nicht geprüft bzw. angegangen.", false, 0}
        }, true, "<b>Tipp:</b> Eine gute Heizung ist notwendig, um zu verhindern, dass kostbare Energie durch schlechte Isolation oder Platzierung verloren geht."},

        {"Beim Kauf Ihrer Geräte achten Sie bzw. Ihre Mitarbeiter:", {
            {"Hauptsächlich auf Geräte mit entsprechenden Energiezertifizierungen.", true, 2},
            {"Nur bedingt darauf.", false, 1},
            {"Gar nicht darauf.", false, 0}
        }, true, "<b>Tipp:</b> Es sollte darauf geachtet werden, möglichst viele energiezertifizierte Geräte zu betreiben, um den Stromverbrauch zu senken."},
    },
}
