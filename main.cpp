#include <string>
#include <string_view>
#include <vector>
#include <sstream>
#include <thread>
#include <chrono>
#include <algorithm>
#include <cstdlib>

#include "RandomGenerator.hpp"
#include "crow_all.h"

using namespace std::literals;



struct Answer {
    std::string_view description;
    bool correct;
    unsigned points = -1;
};
struct Question {
    std::string_view title;
    std::vector<Answer> answers;
    bool radio = false;
    std::string incorrect_text;

    std::string_view getInputType() const {
        return radio?"radio":"checkbox";
    }
};
struct Quiz {
    std::string_view title;
    std::vector<Question> questions;
};


Quiz quiz =
        #include "quiz.inc.cpp"
        ;


void openUrl(const std::string& url) {
    system((
#       ifdef _WIN32
        "start"
#       else
        "xdg-open"
#       endif
           " "+url).c_str());
}

RandomGenerator createRng() {
    RandomGenerator rng;
    rng.seed();
    return rng;
}
RandomGenerator createRng(int seed, int questionIdx) {
    RandomGenerator rng;
    rng.seed(seed+questionIdx);
    return rng;
}

int main() {
    bool isPublic = getenv("QUIZ_PUBLIC");

    // Open website with delay
    std::thread([] () {
        std::this_thread::sleep_for(500ms);
        openUrl("http://localhost:8087");
    }).detach();

    // Set up webserver
    crow::SimpleApp app;

    CROW_ROUTE(app, "/")([] () {
        crow::response fres;
        fres.redirect('/'+std::to_string(createRng().getUInt())+"/0/0");
        return fres;
    });
    CROW_ROUTE(app, "/style.css")([&app] () {
        crow::response fres;
        fres.set_header("Content-Type", "text/css");
        fres.write(
#           include "css.h"
                    );
        return fres;
    });
    CROW_ROUTE(app, "/end")([&app, isPublic] () {
        if (!isPublic) {
            crow::response fres;
            fres.body = "Goodbye.";
            fres.code = crow::status::GONE;
            app.stop();
            return fres;
        } else {
            return crow::response(crow::status::FORBIDDEN);
        }
    });
    CROW_ROUTE(app, "/<int>/<int>/<int>")([isPublic] (const crow::request& req, int seed, int history, int questionIdx) {
        // Add to history
        if (questionIdx-1 < quiz.questions.size()) {
            const auto& question = quiz.questions[questionIdx-1];
            history <<= question.answers.size();
            for (const auto& key : req.url_params.keys()) {
                if (key == "radio") {
                    history |= 1 << std::stoi(req.url_params.get(key));
                }
                try {
                    history |= 1 << std::stoi(key);
                } catch (...) {}
            }
        }
        // Build HTML
        std::ostringstream html;
        html << "<!DOCTYPE html>\n"
                "<html>\n"
                "     <head>\n"
                "         <meta charset='utf-8' />\n"
                "         <title>" << quiz.title << " - Quiz</title>\n"
                "         <link rel='stylesheet' type='text/css' href='/style.css'>"
                "    </head>\n"
                "    <body>\n"
                "        <h1>" << quiz.title << "</h1>\n"
                "        <br>\n" ;
        if (questionIdx < quiz.questions.size()) {
            // Get and show question
            const auto& question = quiz.questions[questionIdx];
            html << "        <div style='outline: 2px dashed var(--accent-bg); padding: 1vw'>\n"
                    "            <h2>" << question.title << "</h2>\n"
                    "            <form action='../" << history << '/' << questionIdx+1 << "' method='GET'>\n";
            // Shuffle answers
            auto answers = question.answers;
            std::shuffle(answers.begin(), answers.end(), createRng(seed, questionIdx).getStd());
            for (unsigned answerIdx = 0; answerIdx != answers.size(); answerIdx++) {
                // Get and show answer
                const auto& answer = answers[answerIdx];
                html << "                <input type='" << question.getInputType() << "' for='answer" << answerIdx << "' name='" << (question.radio?"radio":std::to_string(answerIdx)) << "' value='" << answerIdx << "'>\n"
                        "                <label for='answer" << answerIdx << "'> " << answer.description << "</label><br>\n";
            }
            html << "                <br><input type='submit'>\n"
                    "            </form>\n"
                    "        </div>\n";
        } else {
            std::ostringstream immHtml;
            // Calculate points showing questions and answers in reverse order
            unsigned points = 0, // Current points
                     maxPoints = 0; // Maximum points achievable from the entire quiz
            for (unsigned questionIdx = quiz.questions.size()-1; questionIdx != -1; questionIdx--) {
                // Get and show question
                const auto& question = quiz.questions[questionIdx]; // The question
                immHtml << "        <div style='outline: 2px dashed var(--accent-bg); padding: 1vw'>\n"
                           "            <h2>" << question.title << "</h2>\n"
                           "            <form>\n";
                // Shuffle answers
                auto answers = question.answers; // Shuffled answers to the question
                std::shuffle(answers.begin(), answers.end(), createRng(seed, questionIdx).getStd());
                bool radiocheck = false; // Used to make sure user can only pick one response for radio questions
                unsigned questionMaxPoints = 0; // Maximum points achievable from this question
                bool correctAnswer = false; // Set to true if the user answered this question correctly
                for (unsigned answerIdx = 0; answerIdx != answers.size(); answerIdx++) {
                    // Get user choice (prevent multiple choice on radio question)
                    bool choice = (history & 1) && !radiocheck; // Set if user has selected this answer
                    history >>= 1;
                    radiocheck += choice && question.radio;
                    // Get answer
                    const auto& answer = answers[answerIdx];
                    // Check if user choice was correct and show answer
                    bool correct = choice && answer.correct; // Set if user has selected this answer and this answer is correct
                    correctAnswer += correct;
                    if (answer.points != -1) {
                        questionMaxPoints = std::max(questionMaxPoints, answer.points);
                        points += unsigned(choice) * answer.points;
                    } else {
                        questionMaxPoints = std::max(questionMaxPoints, 1u);
                        points += correct;
                    }
                    // Show answer
                    immHtml << "                <input type='" << question.getInputType() << "' for='answer" << answerIdx << "' disabled" << (choice?" checked":"") << ">\n"
                               "                <label for='answer" << answerIdx << "' style='color: " << ((question.radio?answer.correct:correct)?"forestgreen":"red") << ";'> " << answer.description << "</label><br>\n";
                }
                maxPoints += questionMaxPoints;
                immHtml << "                <p style='color: cornflowerblue;'" << ((!correctAnswer && !question.incorrect_text.empty())?"":" hidden") << ">" << question.incorrect_text << "</p>\n"
                           "            </form>\n"
                           "        </div>\n";
            }
            // Show summary
            html << "        <h2>Summary</h2>\n"
                    "        <p>\n"
                    "            <b>" << points << "</b> / <b>" << maxPoints << "</b> (<b>" << unsigned((float(points)/float(maxPoints))*100.0f) << "%</b>)\n"
                    "        </p><br>\n";
            html << std::move(immHtml).str();
            html << "        <br><br>\n"
                    "        <p><form action='/'><button type='submit'>Retry</button></form>" << (isPublic?"":" <form action='/end'><button type='submit'>End</button></form>") << "</p>\n";
        }
        html << "    </body>\n"
                "</html>\n";
        // Send response
        crow::response fres;
        fres.body = std::move(html).str();
        fres.set_header("Content-Type", "text/html");
        return fres;
    });

    // Run webserver
    try {
        app.port(8087).multithreaded().run();
    } catch (...) {
        // Minor delay
        std::this_thread::sleep_for(750ms);
        // Actual termination
        std::terminate();
    }
}
